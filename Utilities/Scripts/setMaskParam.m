function setMaskParam(blockName,paramName,value)
m = Simulink.Mask.get(blockName);
addr = getMaskParamAddr(blockName,paramName);
m.Parameters(addr).Value = value;
end