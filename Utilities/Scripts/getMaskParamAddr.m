function val = getMaskParamAddr(blockName,paramName)
m = Simulink.Mask.get(blockName);
for i = 1:length(m.Parameters)
    if strcmp(m.Parameters(i).Name,paramName)
        val = i;
        return
    end
    val = 0;
%     error('Parameter "%s" not found in %s.',paramName, blockName);
end