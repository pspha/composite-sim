%% Simulink Model Parameters
tStep = 0.01;
tStop = 500;

simTerminationAlt = -10;
enableSimTermination = true;

% Initial Conditions
x0 = 0;
v0 = 0;

% Sensor Sample Rate
dt = 0.01;


%% Rocket Model Parameters
% Geometry Properties
simSettings.geometry.diameter_in = 8;

simSettings.geometry.frontalArea = ...
    pi * (simSettings.geometry.diameter_in/2)^2 / 1550;

% Aerodynamics Properties
simSettings.aerodynamics.Cd = 0.4;
simSettings.aerodynamics.enableDrag = true;

% Sensor Properties
simSettings.sensors.accelBias = -0.05;
simSettings.sensors.accelNoiseGain = 5;
simSettings.sensors.accelTStep = dt;

simSettings.sensors.makeAccelBodyFrame = true;

simSettings.sensors.baroBias = 0;
simSettings.sensors.baroNoiseGain = 5e-2;
simSettings.sensors.baroMinValue = 10e2;    % From MS5611
simSettings.sensors.baroMaxValue = 1200e2;  % From MS5611
simSettings.sensors.baroTStep = dt;

simSettings.sensors.gpsTStep = 10;
simSettings.sensors.gpsMaxVel = 500;
simSettings.sensors.gpsEnable = true;

simSettings.sensors.ekfTStep = dt;

%%
stage2IgnitionAlt = 8500;
mainChuteAlt = 150;

motor1 = Motor('P8175(SL).ric');
motor2 = Motor('P8751(8k5).ric');

L = 0.1;
M = 0.01;
N = 0.005;

%% Simulation
modelName = 'mainSim';

load_system(modelName)
mdl = modelName;
in = Simulink.SimulationInput(mdl);

out = sim(in);
maxAlt = max(out.position)
maxVel = max(out.velocity);
maxMach = max(out.mach);