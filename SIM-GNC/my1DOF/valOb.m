classdef valOb < matlab.System
    % Value Object
    
    % Public, tunable properties
    properties
        tstep = 0
        preValue = 0
        startValue = 0
        stopValue = 0
        postValue = 0
        stopTime = 0        
    end

    properties(DiscreteState)
        value
        started
    end

    % Pre-computed constants
    properties(Access = private)
        valStep
        
    end

    methods(Access = protected)
        function setupImpl(obj)
            % Perform one-time calculations, such as computing constants
            obj.valStep = (obj.stopValue - obj.startValue) / obj.stopTime * obj.tstep;
        end

        function value = stepImpl(obj,doStep)
            % Implement algorithm. Calculate y as a function of input u and
            % discrete states.
            
             
           if doStep && obj.value == obj.preValue && ~obj.started
               obj.value = obj.startValue;
               obj.started = true;
           end
           
           value = obj.value;
              
          
           if round((obj.value - obj.stopValue)*1e5) == 0
               obj.value = obj.postValue;
           end
           
           if ~doStep || obj.value == obj.postValue
               return
           end
           
          
            obj.value = obj.value + obj.valStep;
      
            
        end

        function resetImpl(obj)
            % Initialize / reset discrete-state properties
            obj.value = obj.preValue;
            obj.started = false;
        end
    end
end
