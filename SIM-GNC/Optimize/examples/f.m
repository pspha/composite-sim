function y = f(x)
a = 2 + 1/3;
b = 3;

y = -0.5 *(x-a).^2 + b;
y = y + (x <= a).*2.^(x-a) + (x > a).*2.^(a-x);