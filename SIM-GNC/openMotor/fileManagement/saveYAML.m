function saveYAML(fileName, data)
    %% Detect Keys
    if any(contains(fields(data),'nameKeys'))
        key = data.nameKeys;
        data = rmfield(data,'nameKeys');
    end
    
    %% Save YAML
    WriteYaml(fileName, data);
    addBackPython(fileName);
    
    if ~exist('key','var')
        fprintf('Nope');
        return
    end
    
    %% Fix Keys
    lines = loadFile(fileName);
    
    keyFields = fields(key);
    
    for i = 1:length(lines)
        for j = 1:length(keyFields)
            if contains(lines{i},keyFields{j})
                lines{i} = strrep(lines{i},keyFields{j},key.(keyFields{j}));
            end
        end
    end
    
    reloadFile(fileName,lines);
end

%% Load File
function lines = loadFile(fileName)
    fid = fopen(fileName, 'r');
    lines = {};
    
    tline = fgetl(fid);
    while ischar(tline)
        lines{end+1} = tline;
        tline = fgetl(fid);
    end
    fclose(fid);
end

%% Reload File
function reloadFile(fileName,lines)
    delete(fileName);
    fid = fopen(fileName, 'w');
    
    for i = 1:length(lines)
        fprintf(fid,'%s\n',lines{i});
    end
    
    fclose(fid);
end


function addBackPython(fileName)
    lines = loadFile(fileName);
    
    %% Helper Functions
    isLine = @(line,target) strcmp(line(1:length(target)),target);
    fixLine = @(line,target,insert) sprintf('%s %s%s',...
        line(1:length(target)),insert,line(length(target)+1:end));
    
    %% Fix Version
    for i = length(lines):-1:1
        if isLine(lines{i},'version:')
            lines{i} = fixLine(lines{i},'version:','!!python/tuple');
            break;
        end
    end
    
    %% Fix Type
    for i = i:-1:1
        if isLine(lines{i},'type:')
            lines{i} = fixLine(lines{i},'type:','!!python/object/apply:uilib.fileIO.fileTypes');
            break;
        end
    end
   
    reloadFile(fileName,lines);
end


    