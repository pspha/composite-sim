function settings = setOMPath(ompath,doError)
    if nargin < 2
        doError = false;
    end

    settings = loadSettings();
    ompath = strrep(ompath,'\','/');
    
    if ~strcmp(ompath(end),'/')
        ompath = strcat(ompath,'/');
    end
    
    settings.OMPATH = ompath;
    [settings.OMPATHisValid,settings.OMPATHisSetup] = checkOMPath(ompath, doError);
    
    saveSettings(settings);

end