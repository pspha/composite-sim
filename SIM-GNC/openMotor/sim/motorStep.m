function [thrust,mass] = motorStep(printTable,printImpulse,ambPressure,tStep,motorStruct)
    persistent m
    persistent imp
    persistent dt
    persistent started
    persistent finished 
    persistent name
    
    
    if nargin == 5
        m = Motor(motorStruct);
        m.config.timestep = tStep;
        m = m.setupSim(ambPressure);
        imp = 0;
        dt = tStep;
        started = false;
        finished = false;
        name = motorStruct.name;
    elseif nargin == 4
        m = m.setupSim(ambPressure);
        imp = 0;
        dt = tStep;
        started = false;
        finished = false;
    elseif nargin == 3
        m = m.stepSim(ambPressure);
    end
    
    simData = m.simRes.channels(end,:);
    thrust = simData.force;
    mass = simData.mass;
    
    imp = imp + thrust * dt;
   
    
    if ~started && thrust > 0
        started = true;
    end
    
    if started && ~finished && thrust == 0
        finished = true;
        
        if printTable
            removevars(m.simRes.channels,{'massPerGrain','massFlowPerGrain','regression','web'})
        end
        
        if printImpulse
            fprintf("%s Impulse: %.1f\n",name,imp/2);
        end
    end
    

end
    