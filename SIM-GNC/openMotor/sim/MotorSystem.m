classdef MotorSystem < matlab.System
    % MotorSystem Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties
        tStep = 0.03;
    end
    properties(Nontunable)
        motorStruct = struct()
        motorName = '';
        printImpulse = false;
        printTable = false;
    end

    properties(DiscreteState)
       motorSetup
       motorStarted
       motorFinished
       drymass
       
    end

    % Pre-computed constants
    properties(Access = private)
        
    end

    methods(Access = protected)
        function setupImpl(obj)
            % Perform one-time calculations, such as computing constants
           
            
        end

        function [thrust,mass] = stepImpl(obj,start,ambPressure)
            % Implement algorithm. Calculate y as a function of input u and
            % discrete states.
            
            thrust = 0;
            mass = 0;
            impulse = 0;
            
            coder.extrinsic('motorStep');
          
            if ~obj.motorSetup
                ms = obj.motorStruct;
                ms.name = obj.motorName;
                [thrust,mass] = motorStep(obj.printTable,obj.printImpulse,ambPressure,obj.tStep,ms);
                obj.drymass = mass;
                obj.motorSetup = true;
               
                if start
                    obj.motorStarted = true;
                    return;
                end
            elseif ~start || obj.motorFinished
                mass = obj.drymass;
                return;
            else
                [~,mass] = motorStep(obj.printTable,obj.printImpulse);
            end
            
            if start && ~obj.motorStarted
                [thrust,mass] = motorStep(obj.printTable,obj.printImpulse,ambPressure,obj.tStep);
                obj.motorStarted = true;
                return;
            end
            
            if obj.motorStarted 
                [thrust,mass] = motorStep(obj.printTable,obj.printImpulse,ambPressure);
                if thrust == 0
                    obj.motorFinished = true;
                    obj.drymass = mass;
                end
                return;
            end
            

           
        end

        function resetImpl(obj)
            % Initialize / reset discrete-state properties
            obj.motorSetup = false;
            obj.motorStarted = false;
            obj.motorFinished = false;
            obj.drymass = 0;
           
        end
    end
end
