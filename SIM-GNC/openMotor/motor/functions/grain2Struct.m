function grainStruct = grain2Struct(grain)
    if iscell(grain)
        grainStruct = {};
        for i = 1:length(grain)
            if ~isa(grain{i},'Grain')
                error('grain{%d} must be of type Grain, not %s.',i, class(grain));
            end
            grainStruct{end+1} = grain{i}.getStruct();
            if ~grain{i}.isValid
                warning('grain{%d} has invalid parameters.',i);
            end
            
        end
    elseif isa(grain,'Grain')
        grainStruct = grain.getStruct();
        if ~grain.isValid
            warning('grain has invalid parameters.');
        end
    else
        error('grain must either be a Grain or an array of Grains, not a %s.',class(grain));
    end
end