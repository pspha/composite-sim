function isValid = checkParamType(value, typeArray, paramName)
    if ismember(value,typeArray)
        isValid = true;
    else
        isValid = false;
        
        if nargin < 3
            paramName = "";
        else
            paramName = sprintf('%s ', strtrim(paramName));
        end
        
        msg = {...
            sprintf('Invalid %sparameter.', paramName),...
            'Valid values are: Neither, Top, Bottom, Both',...
            'Attempted value:'};
        
        error('%s\n\t%s\n\t%s %s', msg{1}, msg{2}, msg{3}, value);
        
    end