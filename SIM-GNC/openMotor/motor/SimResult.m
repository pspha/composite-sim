classdef SimResult
    properties
        channels
        success = false
    end
    properties(Access = private)
        motor
        burnoutThrustThres = 0.1
    end
        
    
    methods
        function sr = SimResult(m)
            numGrains = length(m.grains);
            
            varNames = {'time','kn','pressure','force'};
            
%             grainVarsWithTotal = {'mass','massFlow','massFlux'};
            grainVarsWithTotal = {'mass','massFlow'};
            grainVarsNoTotal = {'regression','web'};
            endVarNames = {'exitPressure','dThroat','ambPressure'};
            
            if numGrains > 1
                for i = 1:length(grainVarsWithTotal)
                    varNames{end+1} = sprintf('%s',grainVarsWithTotal{i});
                    varNames{end+1} = sprintf('%sPerGrain',grainVarsWithTotal{i});
                end
            else
                varNames = cat(2,varNames,grainVarsWithTotal);
            end
            varNames = cat(2,varNames,grainVarsNoTotal,endVarNames);
            
            sr.channels = table('Size',[1,length(varNames)],'VariableTypes',repmat("double",1,length(varNames)));   
            sr.channels.Properties.VariableNames = varNames;
            
            
            sr.motor = m;
        end
        
        function val = getBurnTime(sr)
            val = sr.channels.time(end);
        end
        
        function val = getInitialKN(sr)
            val = sr.channels.kn(1);
        end
        
        function val = getPeakKN(sr)
            val = max(sr.channels.kn);
        end
        
        function val = getAveragePressure(sr)
            val = mean(sr.channels.pressure);
        end
        
        function val = getMaxPressure(sr)
            val = max(sr.channels.pressure);
        end
        
        function val = getImpulse(sr, stop)
            if nargin < 2 || stop == -1
                stop = sr.channels.time(end);
            end
            
            impulse = 0;
            for i = 2:length(sr.channels.time)
                impulse = impulse + sr.channels.force(i) *...
                    (sr.channels.time(i) - sr.channels.time(i-1));
                if sr.channels.time(i) >= stop
                    break;
                end
            end
            
            val = impulse;
        end  
           
        function val = getAverageForce(sr)
            val = mean(sr.channels.force);
        end
        
%         function val = getISP(sr,time)
%             if nargin < 2
%                 
%                 time = -1;
%             end
%             propMass = sr.getPropellantMass() - sr.getPropellantMass(time);
%             
%             
%             if propMass == 0
%                 val = 0;
%             else
%                 val = sr.getImpulse(time) / (propMass * 9.80665);
%             end
%         end
        
        function val = getPropellantLength(sr)
            len = 0;
            for i = 1:length(sr.motor.grains)
                len = len + sr.motor.grains{i}.length;
            end
            val = len;
        end
        
        function val = getPropellantMass(sr,time)
            if nargin < 2
                time = 0;
            end
            
            if time == -1
                time = sr.channels.time(end);
            end
            
            val = max(sr.channels.mass(sr.channels.time >= time));
        end

        function val = shouldContinueSim(sr,thrustThres)
            
            if length(sr.channels.time) == 1
                val = true;
            else
                if nargin < 2
                    thrustThres = sr.motor.config.burnoutThrustThres;
                end
                val = sr.channels.force(end) > thrustThres * 0.01 * max(sr.channels.force);
            end
        end
    end
end