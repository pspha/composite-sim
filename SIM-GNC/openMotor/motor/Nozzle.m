classdef Nozzle
    properties
        throat
        exit
        efficiency
        divAngle
        convAngle
        throatLength
        slagCoeff
        erosionCoeff
    end
    
    methods
        function n = Nozzle(throat,exit,efficiency,divAngle,convAngle,throatLength,slagCoeff,erosionCoeff)
            n.throat = throat;
            n.exit = exit;
            n.efficiency = efficiency;
            n.divAngle = divAngle;
            n.convAngle = convAngle;
            n.throatLength = throatLength;
            n.slagCoeff = slagCoeff;
            n.erosionCoeff = erosionCoeff;
        end
        
         function nozzleStruct = getStruct(n)
            propList = properties(n);
            nozzleStruct = struct();
            for i = 1:(length(propList))
                nozzleStruct.(char(propList(i))) = n.(char(propList(i)));
            end
         end
        
        %% Sim Functions
        function val = calcExpansion(n)
            val = (n.exit / n.throat) ^ 2;
        end
        function val = getThroatArea(n,dThroat)
            if nargin < 2
                dThroat = 0;
            end
            val = geometry.circleArea(n.throat + dThroat);
        end
        function val = getExitArea(n)
            val = geometry.circleArea(n.exit);
        end
        function val = getExitPressure(n,k,inputPressure)
            f = @(x) real(1/n.calcExpansion() - n.eRatioFromPRatio(k,x/inputPressure));

            try
                val = fzero(f,0);
            catch
%                 X = linspace(-1e7,1e7,1000);
%                 Y = [];
%                 for i = 1:length(X)
%                     Y(end+1) = f(X(i));
%                 end
% 
%                 plot(X,Y); hold on;
% 
% 
%                 fprintf("\nP: %.20f\n",inputPressure)
%                 fprintf("\nC: %.20f\n",n.calcExpansion())
%                 fprintf("\nk: %.20f\n",k)
%                 grid on;
%                 error("Invalid");
                val = 0;
            end
        end
        function val = getDivergenceLosses(n)
            divAngleRad = deg2rad(n.divAngle);
            val = (1 + cos(divAngleRad)) / 2;
        end
        function val = getThroatLosses(n,dThroat)
            if nargin < 2
                dThroat = 0;
            end
            throatAspect = n.throatLength / (n.throat + dThroat);
            if throatAspect > 0.45
                val = 0.95;
            else
                val = 0.99 - (0.0333 * throatAspect);
            end
        end
        function val = getSkinLosses(n)
            val = 0.99;
        end
    end
    
    methods (Static)
        function val = eRatioFromPRatio(k, pRatio)
            val = (((k+1)/2)^(1/(k-1))) * (pRatio ^ (1/k)) * ((((k+1)/(k-1))*(1-(pRatio^((k-1)/k))))^0.5);
        end
    end
end
