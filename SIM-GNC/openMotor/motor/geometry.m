classdef geometry
    methods(Static)
        function val = circleArea(dia)
            val = ((dia/2)^2)*pi;
        end
        function val = circlePerimeter(dia)
            val = dia * pi;
        end
        function val = circleDiameterFromArea(area)
            val = 2*((area/pi)^0.5);
        end
        function val = tubeArea(dia,height)
            val = dia * pi * height;
        end
        function val = cylinderArea(dia,height)
            val = (2*circleArea(dia)) + (tubeArea(dia,height));
        end
        function val = cylinderVolume(dia,height)
            val = height*circleArea(dia);
        end
    end
end