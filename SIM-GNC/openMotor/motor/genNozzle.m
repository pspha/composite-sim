function nozzle = genNozzle(convAngle,divAngle,efficiency,erosionCoeff,...
    exit,slagCoeff,throat,throatLength)
    %% Geometry Check
    if throat == 0
        error('Throat diameter must not be 0');
    end
    
    if exit < throat
        error('Exit diameter must not be smaller than throat diameter');
    end
    
    if efficiency == 0
        error('Efficiency must not be 0');
    end

    %% Struct Population
    nozzle = struct();
    nozzle.convAngle = convAngle;
    nozzle.divAngle = divAngle;
    nozzle.efficiency = efficiency;
    nozzle.erosionCoeff = erosionCoeff;
    nozzle.exit = exit;
    nozzle.slagCoeff = slagCoeff;
    nozzle.throat = throat;
    nozzle.throatLength = throatLength;
end