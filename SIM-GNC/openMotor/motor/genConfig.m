function config=genConfig(ambPressure,burnoutThrustThres,burnoutWebThres...
    ,igniterPressure,mapDim,maxMassFlux,maxPressure,minPortThroat,timestep)

    config = struct();
    config.ambPressure = ambPressure;
    config.burnoutThrustThresh = burnoutThrustThres;
    config.burnoutWebThres = burnoutWebThres;
    config.igniterPressure = igniterPressure;
    config.mapDim = mapDim;
    config.maxMassFlux = maxMassFlux;
    config.maxPressure = maxPressure;
    config.minPortThroat = minPortThroat;
    config.timestep = timestep;
end