classdef Motor
    %MOTOR Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        config
        grains
        nozzle
        propellant
        simRes
    end
    
    properties (Access = private)
        type
        version
        isZeroMotor
        
    end
    
    methods
        function m = Motor(arg)
            if nargin > 0
                if ischar(arg)
%                    fprintf('ARG: %s %d\n',arg, isfile(arg));
                   motorStruct = loadYAML(arg);
                end
                
                if isstruct(arg)
                    motorStruct = arg;
                end
                
               m.config = motorStruct.data.config;
               m.grains = struct2Grain(motorStruct.data.grains);
               m.nozzle = struct2Nozzle(motorStruct.data.nozzle);
               m.propellant = struct2Prop(motorStruct.data.propellant);
               m.type = motorStruct.type;
               m.version = motorStruct.version;
               m.isZeroMotor = false;
            else
               m.isZeroMotor = true;
            end
           
        end      
        
        function motorStruct = getStruct(m)
            data = struct();
            if m.isZeroMotor
                motorStruct = data;
                return;
            end
            data.config = m.config;
            data.grains = grain2Struct(m.grains);
            data.nozzle = m.nozzle.getStruct();
            data.propellant = m.propellant.getStruct();
            
            motorStruct = struct();
            motorStruct.data = data;
            motorStruct.type = m.type;
            motorStruct.version = m.version;
        end
           
        function saveMotor(m,fileName)
            if m.isZeroMotor
                warning("Can't save empty motor.");
                return;
            end
            fmt = get(0,'Format');
            format long;
            ms = m.getStruct();
            ms.data.config.ambPressure = sprintf('%.8f',ms.data.config.ambPressure);
            ms.data.config.burnoutWebThres = sprintf('%.20f',ms.data.config.burnoutWebThres);
            ms.data.config.igniterPressure = sprintf('%.8f',ms.data.config.igniterPressure);
            ms.data.config.maxMassFlux = sprintf('%.8f',ms.data.config.maxMassFlux);
            ms.data.config.maxPressure = sprintf('%.8f',ms.data.config.maxPressure);

            format(fmt);
            
            saveYAML(fileName,ms);
        end
        
        % Simulation Functions
        function val = calcKN(m,regDepth,dThroat)
            burnoutThres = m.config.burnoutWebThres;
            regDepth = [regDepth 0];
            
            surfArea = 0;
            for i = 1:length(m.grains)
                gr = m.grains{i};
                reg = regDepth(i);
                surfArea = surfArea + gr.getSurfaceAreaAtRegression(reg) * logical(gr.isWebLeft(reg,burnoutThres));
            end
            
            nozz = m.nozzle.getThroatArea(dThroat);
            val = surfArea / nozz;
        end
        
        function val = calcIdealPressure(m,regDepth,dThroat,lastPressure,kn)
            density = m.propellant.density;
            [ballA,ballN,gamma,temp,molarMass] = m.propellant.getCombustionProperties(lastPressure);
            
            if nargin < 5
                kn = m.calcKN(regDepth,dThroat);
            end
            
            num = kn * density * ballA;
            exponent = 1/(1-ballN);
            denom = ((gamma / ((8314 / molarMass) * temp)) * ((2 / (gamma + 1)) ^ ((gamma + 1) / (gamma - 1)))) ^ 0.5;
            val = (num / denom) ^ exponent;
        end
        
        function val = calcIdealThrustCoeff(m,chamberPres,dThroat,ambPres,exitPres)
            if chamberPres == 0
                val = 0;
                return;
            end
            
            [~,~,gamma,~,~] = m.propellant.getCombustionProperties(chamberPres);
            
            if nargin < 5
                exitPres = m.nozzle.getExitPressure(gamma,chamberPres);
            end
            
            exitArea = m.nozzle.getExitArea();
            throatArea = m.nozzle.getThroatArea(dThroat);
            
            term1 = (2 * (gamma^2)) / (gamma - 1);
            term2 = (2 / (gamma + 1))^((gamma + 1) / (gamma - 1));
            term3 = 1 - ((exitPres / chamberPres) ^ ((gamma - 1) / gamma));
            
            momentumThrust = (term1 * term2 * term3) ^ 0.5;
            pressureThrust = ((exitPres - ambPres) * exitArea) / (throatArea * chamberPres);

            val = momentumThrust + pressureThrust;
        end

        function val = calcForce(m,chamberPres,dThroat,ambPressure,exitPres)
            if nargin < 4
                ambPressure = m.config.ambPressure;
            end
            
            if nargin < 5
                thrustCoeffIdeal = m.calcIdealThrustCoeff(chamberPres,dThroat,ambPressure);
            else
                thrustCoeffIdeal = m.calcIdealThrustCoeff(chamberPres,dThroat,ambPressure,exitPres);
            end
            
            divLoss = m.nozzle.getDivergenceLosses();
            throatLoss = m.nozzle.getThroatLosses(dThroat);
            skinLoss = m.nozzle.getSkinLosses();
            efficiency = m.nozzle.efficiency;
            thrustCoeffAdj = divLoss * throatLoss * efficiency * (skinLoss * thrustCoeffIdeal + (1 - skinLoss));
            thrust = thrustCoeffAdj * m.nozzle.getThroatArea(dThroat) * chamberPres;
            val = max(thrust,0);
        end
        
        function m = setupSim(m,ambPressure,tstep)
            if m.isZeroMotor
                return;
            end
            if nargin > 2
                m.config.timestep = tstep;
            end
            if nargin > 1
                m.config.ambPressure = ambPressure;
            else
                ambPressure = m.config.ambPressure;
            end
            
            sim = SimResult(m);
            
            numGrains = length(m.grains);
            for i = 1:numGrains
                m.grains{i} = m.grains{i}.simulationSetup();
            end
            
            % Initialize Variables
            sim.channels.kn = m.calcKN(zeros(1,numGrains),0);
            igniterPressure = m.config.igniterPressure;
            sim.channels.pressure = m.calcIdealPressure(0,0,igniterPressure,sim.channels.kn);
            
            density = m.propellant.density;
            if numGrains > 1
                sim.channels.massPerGrain = zeros(1,numGrains);
                sim.channels.web = zeros(1,numGrains);
                for i = 1:numGrains
                    sim.channels.massPerGrain(i) = m.grains{i}.getVolumeAtRegression(0) * density;
                    sim.channels.web(i) = m.grains{i}.getWebLeft(0);
                end
                sim.channels.mass = sum(sim.channels.massPerGrain);
                sim.channels.massFlowPerGrain = zeros(1,numGrains);
%                 sim.channels.massFluxPerGrain = zeros(1,numGrains);
                sim.channels.regression = zeros(1,numGrains);
            else
                sim.channels.mass = m.grains{1}.getVolumeAtRegression(0) * density;
                sim.channels.web = m.grains{1}.getWebLeft(0);
            end
            
            sim.channels.ambPressure = ambPressure;
            
            m.simRes = sim;
        end
        
        function m = stepSim(m,ambPressure)
            if m.isZeroMotor
                return;
            end
            if nargin > 1
                m.config.ambPressure = ambPressure;
            else
                ambPressure =  m.config.ambPressure;
            end
            
            dTime = m.config.timestep;
            
            if m.simRes.shouldContinueSim(m.config.burnoutThrustThres)
                density = m.propellant.density;
                last = m.simRes.channels(end,:);
                
                warning('off','MATLAB:table:RowsAddedExistingVars');
                m.simRes.channels.time(end+1) = round( m.simRes.channels.time(end) + dTime,6);
                warning('on','MATLAB:table:RowsAddedExistingVars');
                
                reg = dTime * m.propellant.getBurnRate(last.pressure);
                numGrains = length(m.grains);
                if numGrains > 1
                    for i = 1:numGrains
%                         m.simRes.channels.massFluxPerGrain(end,i) = m.grains{i}.getPeakMassFlux(...
%                             last.massFlowPerGrain(i),dTime,last.regression(i),reg,density);
                        m.simRes.channels.massPerGrain(end,i) = m.grains{i}.getVolumeAtRegression(last.regression(i))*density;    
                        m.simRes.channels.massFlowPerGrain(end,i) = (last.massPerGrain(i) - m.simRes.channels.massPerGrain(end,i)) / dTime;
                            
                        m.simRes.channels.mass(end) = sum(m.simRes.channels.massPerGrain(end,:));
%                         m.simRes.channels.massFlux(end) = sum(m.simRes.channels.massFluxPerGrain(end,:));
                        m.simRes.channels.massFlow(end) = sum(m.simRes.channels.massFlowPerGrain(end,:));
                        
                        m.simRes.channels.regression(end,i) = last.regression(i) + reg;
                        m.simRes.channels.web(end,i) = m.grains{i}.getWebLeft(m.simRes.channels.regression(end,i));
                        
                    end
                    
                    regression = m.simRes.channels.regression(end,:);
                else
%                     m.simRes.channels.massFlux(end) = m.grains{1}.getPeakMassFlux(...
%                             last.massFlow,dTime,last.regression,reg,density);
                    m.simRes.channels.mass(end) = m.grains{1}.getVolumeAtRegression(last.regression)*density;    
                    m.simRes.channels.massFlow(end) = (last.mass - m.simRes.channels.mass(end)) / dTime;
                    m.simRes.channels.regression(end) = last.regression + reg;
                    m.simRes.channels.web(end) = m.grains{1}.getWebLeft(m.simRes.channels.regression(end));
                    regression = m.simRes.channels.regression(end);
                end
                
                
                % Caclulate KN
                dThroat = last.dThroat;
                kn = m.calcKN(regression,dThroat);
                m.simRes.channels.kn(end) = kn;
                
                % Calculate Pressure
                pressure = m.calcIdealPressure(regression,dThroat,last.pressure,kn);
                m.simRes.channels.pressure(end) = pressure;
                
                % Calculate Exit Pressure
                [~,~,gamma,~,~] = m.propellant.getCombustionProperties(pressure);
                exitPressure = m.nozzle.getExitPressure(gamma,pressure);
                m.simRes.channels.exitPressure(end) = exitPressure;
                
                % Calculate Force
                force = m.calcForce(pressure,dThroat,ambPressure,exitPressure);
                m.simRes.channels.force(end) = force;
                
                % Calculate any slag deposition or erosion of the throat
                if pressure == 0
                    slagRate = 0;
                else
                    slagRate = (1 / pressure) * m.nozzle.slagCoeff;
                end
                erosionRate = pressure * m.nozzle.erosionCoeff;
                change = dTime * ((-2 * slagRate) + (2 * erosionRate));
                m.simRes.channels.dThroat(end) = dThroat + change;
                
                m.simRes.channels.ambPressure(end) = ambPressure;
                
            elseif m.simRes.success == false
                last = m.simRes.channels(end,:);
                warning('off','MATLAB:table:RowsAddedExistingVars');
                m.simRes.channels.time(end+1) = round( m.simRes.channels.time(end) + dTime,6);
                warning('on','MATLAB:table:RowsAddedExistingVars');
                
                m.simRes.channels.mass(end) = last.mass;
                m.simRes.channels.dThroat(end) = last.dThroat;
                m.simRes.channels.ambPressure(end) = ambPressure;
                
                if length(m.grains) > 1
                    m.simRes.channels.regression(end,:) = last.regression;
                    m.simRes.channels.web(end,:) = last.web;
                   
                else
                    m.simRes.channels.regression(end) = last.regression;
                    m.simRes.channels.web(end) = last.web;
                   
                end
                
                m.simRes.success = true;
            end
            
        end
        
        function val = simComplete(m)
            if m.isZeroMotor
                val = true;
            else
                val = ~m.simRes.shouldContinueSim(m.config.burnoutThrustThres) && m.simRes.success;
            end
        end
    end
end

