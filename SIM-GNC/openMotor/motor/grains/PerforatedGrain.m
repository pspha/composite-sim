classdef (Abstract) PerforatedGrain < Grain
    properties(Access = protected)
        geoName = 'perfGrain'
        wallWeb = 0
    end
    properties (Abstract)
        inhibitedEnds
        length
    end
   
    methods (Abstract)
        val = getCorePerimeter(g,regDist)
        val = getFaceArea(g,regDist)
    end
    
    methods
        function val = getEndPositions(g,regDist)
            if strcmp(g.inhibitedEnds,'Neither')
                val = [regDist, g.length - regDist];
            elseif strcmp(g.inhibitedEnds,'Top')
                val = [0, g.length - regDist];
            elseif strcmp(g.inhibitedEnds,'Bottom')
                val = [regDist, g.length];
            elseif strcmp(g.inhibitedEnds,'Both')
                val = [0, g.length];
            else
                error('Invalid number of faces inhibited');
            end
        end
        
        function val = getCoreSurfaceArea(g,regDist)
            corePerimeter = g.getCorePerimeter(regDist);
            coreArea = corePerimeter * g.getRegressedLength(regDist);
            val = coreArea;
        end
        
        function val = getWebLeft(g,regDist)
            wallLeft = g.wallWeb - regDist;
            lengthLeft = g.getRegressedLength(regDist);
            
            val = min(lengthLeft, wallLeft);
        end
        
        function val = getSurfaceAreaAtRegression(g,regDist)
            faceArea = g.getFaceArea(regDist);
            coreArea = g.getCoreSurfaceArea(regDist);
            
            exposedFaces = 2;
            if strcmp(g.inhibitedEnds,'Top') || strcmp(g.inhibitedEnds,'Bottom')
                exposedFaces = 1;
            elseif strcmp(g.inhibitedEnds,'Both')
                exposedFaces = 0;
            end
            
            val = coreArea + (exposedFaces * faceArea);
        end
        
        function val = getVolumeAtRegression(g,regDist)
            faceArea = g.getFaceArea(regDist);
            val = faceArea * g.getRegressedLength(regDist);
        end
        
        function val = getPortArea(g,regDist)
            faceArea = g.getFaceArea(regDist);
            uncored = geometry.circleArea(g.diameter);
            val = uncored - faceArea;
        end
        
        function val = getMassFlux(g,massIn,dTime,regDist,dRegDist,position,density)
            diameter = g.diameter;
            
            endPos = g.getEndPositions(regDist);
            
            if position < endPos(1)
                val = massIn / geometry.circleArea(diameter);
                return;
            end
            
            if position <= endPos(2)
                if strcmp(g.inhibitedEnds,'Top')
                    top = 0;
                    countedCoreLength = position;
                else
                    top = g.getFaceArea(regDist + dRegDist) * dRegDist * density;
                    countedCoreLength = position - (endPos(1) + dRegDist);
                end
                
                core = ((g.getPortArea(regDist + dRegDist) * countedCoreLength) - (g.getPortArea(regDist) * countedCoreLength));
                core = core * density;
                
                massFlow = massIn + ((top + core) / dTime);
                val = massFlow / g.getPortArea(regDist + dRegDist);
                return;
            end
            
            massFlow = massIn + (g.getVolumeSlice(regDist, dRegDist) * density / dTime);
            val = massFlow / geometry.circleArea(diameter);
        end
    end
end