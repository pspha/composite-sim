classdef EndBurner < Grain
   properties
       diameter
       length
   end
    
   methods
       function g = EndBurner(diameter,length)
           g.diameter = diameter;
           g.length = length;
       end
       function grainType = type(g)
           grainType = 'End Burner';
       end
       function isValid = check(g)
           isValid = true;
       end
   end
end