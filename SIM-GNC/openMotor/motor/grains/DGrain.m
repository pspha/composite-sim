classdef DGrain < Grain
    properties
        diameter
        inhibitedEnds
        length
        slotOffset
    end

    methods
        function g = DGrain(diameter,inhibitedEnds,length,slotOffset)
            g.diameter = diameter;
            g.inhibitedEnds = inhibitedEnds;
            g.length = length;
            g.slotOffset = slotOffset;
        end
        function grainType = type(g)
            grainType = 'D Grain';
        end
        function isValid = check(g)
            isValid = g.checkDGrain(g.diameter,g.inhibitedEnds,g.length,g.slotOffset);
        end
        
    end
    methods (Static)
        function isValid = checkDGrain(diameter,inhibitedEnds,length,slotOffset)
            isValid = Grain.checkInhibitedEnds(inhibitedEnds);
            
            if slotOffset > diameter / 2;
                warning('Core offset must not be greater than grain radius');
                isValid = false;
            end
            if slotOffset < -diameter / 2;
                warning('Core offset must be greater than negative grain radius');
                isValid = false;
            end
        end
    end
end