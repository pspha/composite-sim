classdef CGrain < Grain
    properties
        diameter
        inhibitedEnds
        length
        slotOffset
        slotWidth
    end

    methods
        function g = CGrain(diameter,inhibitedEnds,length,slotOffset,slotWidth)
            g.diameter = diameter;
            g.inhibitedEnds = inhibitedEnds;
            g.length = length;
            g.slotOffset = slotOffset;
            g.slotWidth = slotWidth;
        end
        function grainType = type(g)
            grainType = 'C Grain';
        end
        function isValid = check(g)
            isValid = g.checkCGrain(g.diameter,g.inhibitedEnds,g.length,g.slotOffset,g.slotWidth);
        end
        
    end
    methods (Static)
        function isValid = checkCGrain(diameter,inhibitedEnds,length,slotOffset,slotWidth)
            isValid = Grain.checkInhibitedEnds(inhibitedEnds);
            
            if slotOffset > diameter / 2;
                warning('Slot offset should be less than grain radius');
                isValid = false;
            end
            if slotOffset < -diameter / 2;
                warning('Slot offset should be greater than negative grain radius');
                isValid = false;
            end
            
            if slotWidth == 0
                warning('Slot width must not be 0');
                isValid = false;
            end
            if slotWidth > diameter
                warning('Slot width should not be greater than grain diameter');
                isValid = false;
            end
        end
    end
end