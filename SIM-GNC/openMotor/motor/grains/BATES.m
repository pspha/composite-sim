classdef BATES < PerforatedGrain
    properties
        coreDiameter
        diameter
        inhibitedEnds
        length
    end

    methods
        function g = BATES(coreDiameter,diameter,inhibitedEnds,length)
            g.coreDiameter = coreDiameter;
            g.diameter = diameter;
            g.inhibitedEnds = inhibitedEnds;
            g.length = length;
        end
        function grainType = type(g)
            grainType = 'BATES';
        end
        function isValid = check(g)
            isValid = g.checkBATES(g.coreDiameter,g.diameter,g.inhibitedEnds,g.length);
        end
        
        % Simulation Functions
        function g = simulationSetup(g, config)
            g.wallWeb = (g.diameter - g.coreDiameter) / 2;
            g.simIsSetup = true;
        end
        
        function val = getCorePerimeter(g,regDist)
            val = geometry.circlePerimeter(g.coreDiameter + (2 * regDist));
        end
        
        function val = getFaceArea(g, regDist)
            outer = geometry.circleArea(g.diameter);
            inner = geometry.circleArea(g.coreDiameter + (2 * regDist));
            val = outer - inner;
        end
        
    end
    methods (Static)
        function isValid = checkBATES(coreDiameter,diameter,inhibitedEnds,length)
            isValid = Grain.checkInhibitedEnds(inhibitedEnds);
            if coreDiameter == 0
                warning('Core diameter must not be 0.');
                isValid = false;
            end
            if coreDiameter >= diameter
                warning('Core diameter must be less than grain diameter.');
                isValid = false;
            end
        end
            
    end
end