classdef XCore < Grain
    properties
        diameter
        inhibitedEnds
        length
        slotLength
        slotWidth
    end
    
    methods
        function g = XCore(diameter,inhibitedEnds,length,slotLength,slotWidth)
            g.diameter = diameter;
            g.inhibitedEnds = inhibitedEnds;
            g.length = length;
            g.slotLength = slotLength;
            g.slotWidth = slotWidth;
        end
        function grainType = type(g)
            grainType = 'X Core';
        end
        function isValid = check(g)
            isValid = g.checkXCore(g.diameter,g.inhibitedEnds,g.length,g.slotLength,g.slotWidth);
        end
    end
    methods (Static)
        function isValid = checkXCore(diameter,inhibitedEnds,length,slotLength,slotWidth)
            isValid = Grain.checkInhibitedEnds(inhibitedEnds);
            if slotWidth == 0
                warning('Slot width must not be 0.');
                isValid = false;
            end
            if slotWidth > diameter
                warning('Slot width should be less than or equal to grain diameter.');
                isValid = false;
            end

            if slotLength == 0
                warning('Slot length must not be 0');
                isValid = false;
            end
            if slotLength * 2 > diameter
                warning('Slot length should be less than or equal to grain radius.');
                isValid = false;
            end
        end
    end
end