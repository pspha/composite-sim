classdef Propellant
    properties
        density
        name
        tabs
        editsEnabled
    end
    properties (Access = private)
        hasEditsVar
    end
        
    
    methods
        %% Constructors
        function p = Propellant(name)
            p.editsEnabled = true;
            p.hasEditsVar = false;
            
            if nargin == 0
                return;
            end
            
            [props,propPath,~] = Propellant.loadProps();
            for i = 1:length(props.data)
                if strcmpi(strrep(props.data{i}.name,' ',''),strrep(name,' ','')) % Case and whitespace insensitive
                    p.density = props.data{i}.density;
                    p.name = props.data{i}.name;
                    p.tabs = props.data{i}.tabs;
                    
                    p.editsEnabled = false;
                    p.hasEditsVar = false;
                    return
                end
            end
            
            error('Propellant "%s" not found in propellants.yaml at path "%s"', name, propPath);
        end
        
        %% Sim Methods
        function val = getCStar(p,pressure)
            [~,~,gamma,temp,molarMass] = p.getCombustionProperties(pressure);
            gasConst = 8314;
            num = (gamma * gasConst / molarMass * temp) ^ 0.5;
            denom = gamma * ((2 / (gamma + 1))^((gamma + 1) / (gamma - 1))) ^ 0.5;
            val = num / denom;
        end
        function val = getBurnRate(p,pressure)
            [ballA,ballN,~,~,~] = p.getCombustionProperties(pressure);
            val = ballA * (pressure ^ ballN);
        end
        function [a,n,k,t,m] = getCombustionProperties(p,pressure)
            closestPressure = 1e100;
            for i = 1:length(p.tabs)
                tab = p.tabs{i};
                if tab.minPressure < pressure && pressure < tab.maxPressure
                    closest = tab;
                    break;
                end
                if abs(pressure - tab.minPressure) < closestPressure
                    closest = tab;
                    closestPressure = abs(pressure - tab.minPressure);
                end
                if abs(pressure - tab.maxPressure) < closestPressure
                    closest = tab;
                    closestPressure = abs(pressure - tab.maxPressure);
                end
                
            end
            a = closest.a;
            n = closest.n;
            k = closest.k;
            t = closest.t;
            m = closest.m;
        end
        %% Setter Methods
        function p = set.density(p,value)
            p.checkEditable('density');
            if p.density ~= value
                p.hasEditsVar = true;
            end
            p.density = value;
        end
        function p = set.name(p,value)
            p.checkEditable('name');
            if ~strcmp(p.name,value)
                p.hasEditsVar = true;
            end
            p.name = value;
        end
        function p = set.tabs(p,value)
            p.checkEditable('tabs');
            if ~iscell(p.tabs) 
                p.hasEditsVar = true;
            elseif length(p.tabs) ~= length(value)
                p.hasEditsVar = true;
            elseif ~cellfun(@isequal,p.tabs,value)
                p.hasEditsVar = true;
            end
            p.tabs = value;
        end
        
        function p = set.hasEditsVar(p,value)
            p.hasEditsVar = value;
        end
        
        %% Getter Methods
        function value = hasEdits(p)
            value = p.hasEditsVar;
        end
        
        function propStruct = getStruct(p)
            propStruct = struct();
            propStruct.density = p.density;
            propStruct.name = p.name;
            propStruct.tabs = p.tabs;
        end
            
        
        %% Custom Prop Creation
        function p = addNewTab(p, a,k,m,maxPressure,minPressure,n,t)
            newTab = struct();
            newTab.a = a;
            newTab.k = k;
            newTab.m = m;
            newTab.maxPressure = maxPressure;
            newTab.minPressure = minPressure;
            newTab.n = n;
            newTab.t = t;
            
            p.tabs{end+1} = newTab;
        end
        
        function p = duplicateTab(p,n)
            p.tabs{end+1} = p.tabs{n};
        end
        
        function p = save(p)
           [props,~,propFileName] = Propellant.loadProps();
           
           for i = 1:length(props.data)
               if strcmp(props.data{i}.name,p.name)
                   if p.overwriteConfirm(p.name)
                       props.data{i} = p.getStruct();
                       p = p.savePropsFile(props,propFileName);
                   end
                   return
               end
           end
           
           props.data{i+1} = p.getStruct();
           p.savePropsFile(props,propFileName);
        end
        
    end   
    
    %% Private Methods
    methods (Access = private)
        function checkEditable(p,propertyName)
            msgLine1 = sprintf('Unable to edit Propellant.%s due because Propellant edits are not currently enabled.',propertyName);
            msgLine2 = 'To enable edits, set Propellant.editsEnabled = true';
                        
            if ~p.editsEnabled
                error('%s\n%s',msgLine1,msgLine2);
            end
        end
        
        function doOverwrite = overwriteConfirm(p,propName)
            msg = sprintf('"%s" %s\n%s',...
                propName, 'already exists in propellants.yaml.',...
                'Are you sure you want to overwrite this entry?');
            
            answer = questdlg(msg,...
                     'Overwrite Propellant Data?',...
                     'Yes', 'No','No');
                 
            switch answer
                case 'Yes'
                    doOverwrite = true;
                case 'No'
                    doOverwrite = false;
            end
        end
        
        function p = savePropsFile(p,props,propsFileName)
            delete(propsFileName);
            saveYAML(propsFileName,props);
            p.hasEditsVar = false;
        end
    end
    
    %% Static Methods
    methods (Static)
        function [props,propPath,propFileName] = loadProps()
            settings = loadSettings();
            
            if strcmp(settings.APIPATH(end-13:end),'/openMotorAPI/')
                propPath = settings.APIPATH(1:end-13);
            else
                error('Invalid APIPATH from settings file.');
            end
            
            propFileName = strcat(propPath,'propellants.yaml');
            
            if isfile(propFileName)
                props = loadYAML(propFileName);
            else
                error('Unable to find propellants.yaml at path: "%s"', propFileName);
            end
        end
    end
end