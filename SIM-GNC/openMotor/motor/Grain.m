classdef (Abstract) Grain
    properties 
        isValid
    end
    
    properties(Access = protected)
        simIsSetup = false
    end
    
    methods (Abstract)
        grainType = type(g)
        isValid = check(g)
        % Simulation Methods
        val = getSurfaceAreaAtRegression(g,regDist)
        val = getVolumeAtRegression(g,regDist)
        val = getWebLeft(g,regDist)
        val = getMassFlux(g, massIn, dTime, regDist, dRegDist, position, density)
        val = getEndPositions(g,regDist)
        val = getPortArea(g,regDist)
        
    end
    methods
        function isValid = get.isValid(g) 
            isValid = g.check();
        end
        
        function simIsSetup = isSimSetup(g)
            simIsSetup = g.simIsSetup;
        end
        
        function grainStruct = getStruct(g)
            propList = properties(g);
            propStruct = struct();
            for i = 1:(length(propList)-1)
                propStruct.(char(propList(i))) = g.(char(propList(i)));
            end
            grainStruct = struct();
            grainStruct.properties = propStruct;
            grainStruct.type = g.type;
        end
        
        % Simulation Methods
        function val = getVolumeSlice(g,regDist,dRegDist)
            val = g.getVolumeAtRegression(regDist) - getVolumeAtRegression(regDist + dRegDist);
        end
        function val = isWebLeft(g,regDist,burnoutThres)
            if nargin < 3
                burnoutThres = 0.00001;
            end
            val = g.getWebLeft(regDist) > burnoutThres;
        end
        function val = getPeakMassFlux(g,massIn,dTime,regDist,dRegDist,density)
            endPositions = g.getEndPositions(regDist);
            val = g.getMassFlux(massIn,dTime,regDist,dRegDist,endPositions(2),density);
        end
        function val = getRegressedLength(g,regDist)
            endPos = g.getEndPositions(regDist);
            val = endPos(2) - endPos(1);
        end
    
    end
    methods (Static)
        function isValid = checkInhibitedEnds(inhibitedEnds)
            validInhibitedEnds = {'Neither','Top','Bottom','Both'};
            if isnumeric(inhibitedEnds)
                error('inhibitedEnds parameter must not be numeric.');
            end
            
            if ismember(inhibitedEnds,validInhibitedEnds)
                isValid = true;
            else
                isValid = false;
                msg = {...
                    'Invalid inhibitedEnds parameter.',...
                    'Valid values are: Neither, Top, Bottom, Both',...
                    'Attempted value:'};

                error('%s\n\t%s\n\t%s %s', msg{1}, msg{2}, msg{3}, value);
            end
        end
    end
end