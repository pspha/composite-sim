%% Setup Variables
tStep = 0.03;

% m = Motor('test.ric');
% m = Motor('test2grain.ric');
motor = Motor('P6143.ric');

ambPressure = motor.config.ambPressure;

doSimulink = true;
% doMatlab = true;


%% Run Simulation in Simulink
if doSimulink
    modelName = 'exampleSim';

    load_system(modelName)
    mdl = modelName;
    in = Simulink.SimulationInput(mdl);

    out = sim(in);
end
%% Run Simulation in Matlab
if doMatlab
    motor.config.timestep = tStep;
    motor = motor.setupSim();

    while ~motor.simComplete()
        motor = motor.stepSim();
    %     m = m.stepSim(ambPressure)
    end

%     m.simRes.channels
%     m.simRes.getImpulse()
end