%% Load Python Environment 
cd('..')
addpath(genpath('sizing-algorithm'))
cd('sizing-algorithm')
settings = loadSettings();
pe = pyenv;
executable = sprintf('%s.venv/Scripts/python.exe',settings.OMPATH);
if ~strcmp(pe.Executable,strrep(executable,'/','\'))
    pyenv('Version',executable);
end

%% Example Simulation
m = Motor('test.ric');
ms = MotorSim(m);

while ~ms.isComplete
    ms = ms.stepSim()
end

ms.simData