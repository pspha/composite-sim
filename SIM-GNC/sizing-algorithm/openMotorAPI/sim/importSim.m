function motorSim = importSim(filename)
    warning('off','MATLAB:table:ModifiedAndSavedVarnames');
    motorSim = readtable(filename); % Motor Table
    
    
    warning('on','MATLAB:table:ModifiedAndSavedVarnames');
end