classdef MotorSim
    properties
        pyMotor
        simData
    end
    properties (Access = private)
        simStarted
    end
    methods
        function ms = MotorSim(m)
            if ~isa(m,'Motor')
                error("MotorSim's must be constructed with a Motor instance");
            end
            
            currentPath = pwd;
            settings = loadSettings();
            cd(settings.OMPATH);
            oM = py.importlib.import_module('om');
            cd(currentPath);
            ms.pyMotor = oM.motor.Motor();
            ms.pyMotor.applyDict(struct2dict(py.dict,m.getStruct.data));
            
            ms.simStarted = false;
        end  
        
        function ms = stepSim(ms,ambPressure) 
            if ~ms.simStarted
                
                if nargin > 1
                    ms.pyMotor.startSimulation(ambPressure);
                else
                    ms.pyMotor.startSimulation();
                end
                
                ms.simStarted = true;
                data = cell2mat(cell(ms.pyMotor.getSimData()));
                names = string(cell(py.list(ms.pyMotor.getSim.channels)));
                
                ms.simData = array2table(data);
                ms.simData.Properties.VariableNames = names;
            elseif logical(ms.pyMotor.getSimShouldContinue())
                
                if nargin > 1
                    ms.pyMotor.stepSimulation(ambPressure);
                else
                    ms.pyMotor.stepSimulation();
                end
                data = (cell(ms.pyMotor.getSimData()));
                ms.simData = [ms.simData;data];
            else
                warning('OpenMotor Sim is complete. No timestep performed.');
            end 
        end
        
        function value = isComplete(ms)
            value = ~logical(ms.pyMotor.getSimShouldContinue());
        end
    end
end

function dict = struct2dict(dict,dataStruct)
    dataStructFields = fields(dataStruct);
    
    for i = 1:length(dataStructFields)
        if isstruct(dataStruct.(dataStructFields{i}))
            dict{dataStructFields{i}} = struct2dict(py.dict,dataStruct.(dataStructFields{i}));
        else
            dict{dataStructFields{i}} = dataStruct.(dataStructFields{i});
        end
    end
end

