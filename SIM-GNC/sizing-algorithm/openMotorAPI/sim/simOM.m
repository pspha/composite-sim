function simOM(motorFile,outputFile)
    settings = loadSettings();

    outputFile = strrep(outputFile,'\','/');
    motorFile = strrep(motorFile,'\','/');
    
    actCmd = strcat(settings.OMPATH,'.venv/Scripts/activate.bat');

    simCmd = sprintf('python %smain.py -h -o %s %s', settings.OMPATH, outputFile, motorFile);
    
    deactCmd = strcat(settings.OMPATH,'.venv/Scripts/deactivate.bat');
    cmd = sprintf('%s & %s & %s', actCmd, simCmd, deactCmd);
    
    system(cmd);

end