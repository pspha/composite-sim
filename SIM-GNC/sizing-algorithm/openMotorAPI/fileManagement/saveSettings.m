function saveSettings(settings,fileName)
    if nargin < 2
        fileName = '.settings.json';
    end
    
    if isfile(fileName)
        delete(fileName);
    end
    
    js = jsonencode(settings);
    fid = fopen(fileName,'w');
    fprintf(fid,js);
    fclose(fid);
    
    fileattrib(fileName,'+h'); 
end