function [isValid,isSetup] = checkOMPath(ompath,doError)
    isValid = true;
    isSetup = false;
    
    if nargin < 2
        doError = false;
    end
    
    % Use forwards slashes
    ompath = strrep(ompath,'\','/');
    
    % Check if folder exists
    if ~isfolder(ompath)
        isValid = false;
        msg = sprintf('Cannot find openMotor path: "%s"', ompath);
        alert(msg,doError);
        return
    end
    
    % Ensure slash at end of path
    if ~strcmp(ompath,'/')
        ompath = strcat(ompath,'/');
    end
    
    % Check for motorlib
    if ~isfolder(strcat(ompath,'motorlib'))
        isValid = false;
        msg = sprintf('Cannot find motorlib folder at path: "%s"', ompath);
        alert(msg,doError);
        return
    end
    
    % Check for uilib
    if ~isfolder(strcat(ompath,'uilib'))
        isValid = false;
        msg = sprintf('Cannot find uilib folder at path: "%s"', ompath);
        alert(msg,doError);
        return
    end
    
    % Check for app.py
    if ~isfile(strcat(ompath,'app.py'))
        isValid = false;
        msg = sprintf('Cannot find app.py at path: "%s"', ompath);
        alert(msg,doError);
        return
    end
    
    % Check for main.py
    if ~isfile(strcat(ompath,'main.py'))
        isValid = false;
        msg = sprintf('Cannot find main.py at path: "%s"', ompath);
        alert(msg,doError);
        return
    end    
    
    % Check for virtual environment
    isSetup = isfolder(strcat(ompath,'.venv'));
    
    if ~isSetup
        msg = 'Path is valid, but virtual environment ".venv" is not setup';
        warning('%s\nPath: %s', msg, ompath);
    end
end

function alert(msg,doError)
    if doError
        error(msg)
    else
        warning(msg)
    end
end