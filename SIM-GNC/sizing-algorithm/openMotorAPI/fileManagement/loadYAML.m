function yamlData = loadYAML(fileName)
    %% Remove Existing Temp Files
    tempFile = 'temp.txt';
    if isfile(tempFile)
        delete(tempFile);
    end
    
    %% Load YAML File
    fYAML = fopen(fileName, 'r');
    fTEMP = fopen(tempFile, 'w');
    
    yamlData.fid = fTEMP;
    
    key = struct();
    
    % Get YAML line by line
    tline = fgetl(fYAML);
    while ischar(tline)
        tline = erase(tline,'!!python/object/apply:uilib.fileIO.fileTypes');
        tline = erase(tline,'!!python/tuple');
        
        [tline,key] = fixFieldNames(tline,key);
        
        fprintf(fTEMP,'%s\n',tline);
        tline = fgetl(fYAML);
    end
    
    
    yamlData = ReadYaml(tempFile);
    
        
    
    if length(fields(key)) > 0
        yamlData.nameKeys = key;
    end
    
    %% Close YAML File
    fclose(fYAML);
    fclose(fTEMP);
    
    yamlData.type = makeCellArrayInts(yamlData.type);
    yamlData.version = makeCellArrayInts(yamlData.version);
    
    
    delete(tempFile);
end

function [line,key] = fixFieldNames(line,key)
    i = strfind(line,':');
    
    newKeys = {};
    newVals = {};
    
    prev = 1;
    
    if any(i)
        for stopAddr = i
            name = line(prev:stopAddr);
            startAddr = max([strfind(name,'{'),strfind(name,' '),0]);
            name = name(startAddr+1:end-1);
            
            safeName = strrep(name,'/','_');
            safeName = erase(safeName,'(');
            safeName = erase(safeName,')');
            safeName = strrep(safeName,'*','x');
            safeName = strrep(safeName,'^','TO');
            
%             fprintf('%s -> %s\n', name, safeName);
            if ~strcmp(name,safeName)
                newKeys{end+1} = safeName;
                newVals{end+1} = name;
            end
                        
            prev = stopAddr;
            

        end
    end
    
    for i = 1:length(newKeys)
        key.(newKeys{i}) = newVals{i};
        line = strrep(line,newVals{i},newKeys{i});
    end
    
end
    
function cellArray = makeCellArrayInts(cellArray)
    for i = 1:length(cellArray)
        cellArray{i} = int8(cellArray{i});
    end
end
    