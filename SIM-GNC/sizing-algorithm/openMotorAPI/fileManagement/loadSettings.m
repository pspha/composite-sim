function settings = loadSettings(fileName)
    settingsPath = getSettingsPath();
    
    if nargin < 1
        settingsFileName = '.settings.json';
        fileName = strcat(erase(settingsPath, 'openMotorAPI/'),settingsFileName);
    end
    
    validFileExists = isfile(fileName);
    
    if validFileExists
%         try
            fid = fopen(fileName,'r');
            raw = fread(fid,inf);
            str = strrep(char(raw'),'\','/');
            settings = jsondecode(str);
            fclose(fid); 
            settings.APIPATH = settingsPath;
            saveSettings(settings,fileName);

%         catch
%             validFileExists = false;
%             fclose(fid); 
%         end
    end
    
    if ~validFileExists
        settings = struct();
        settings.APIPATH = settingsPath;
        
        saveSettings(settings);        
    end
end

function shortenedPath = getShortenedPath(currentPath,target)
    shortenedPathLength = strfind(currentPath,target) + length(target);
    if shortenedPathLength < length(currentPath) 
        shortenedPath = currentPath(1:shortenedPathLength);
    else
        shortenedPath = currentPath;
    end
end

function settingsPath = getSettingsPath()
    apiTarget = 'openMotorAPI';
    gitTarget = 'sizing-algorithm';
    settingsPath = strrep(pwd,'\','/');
    
    if contains(settingsPath,apiTarget)
        settingsPath = getShortenedPath(settingsPath,apiTarget);
    elseif contains(settingsPath,gitTarget)
        settingsPath = getShortenedPath(settingsPath,gitTarget);
        settingsPath = sprintf('%s/%s',settingsPath,apiTarget);
    else
        error('Unable to trace current path to "sizing-algorith/openMotorAPI');
    end
    
    if ~strcmp(settingsPath(end),'/')
        settingsPath = strcat(settingsPath,'/');
    end
    
%     settingsPath = erase(settingsPath, 'openMotorAPI/');
end