%% Load Python Environment 
cd('..')
addpath(genpath('sizing-algorithm'))
cd('sizing-algorithm')
settings = loadSettings();
pe = pyenv;
executable = sprintf('%s.venv/Scripts/python.exe',settings.OMPATH);
if ~strcmp(pe.Executable,strrep(executable,'/','\'))
    pyenv('Version',executable);
end
cd(settings.OMPATH)

oM = py.importlib.import_module('om');

cd(settings.APIPATH)
cd('..')

%% Load Motor Settings
% m = oM.motor.Motor();
% 
% M = Motor('test.ric');
% m.applyDict(M.getPyDict());