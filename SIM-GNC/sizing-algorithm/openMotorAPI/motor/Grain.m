classdef (Abstract) Grain
    properties 
        isValid
    end
    
    methods (Abstract)
        grainType = type(g)
        isValid = check(g)
    end
    methods
        function isValid = get.isValid(g) 
            isValid = g.check();
        end
        
        function grainStruct = getStruct(g)
            propList = properties(g);
            propStruct = struct();
            for i = 1:(length(propList)-1)
                propStruct.(char(propList(i))) = g.(char(propList(i)));
            end
            grainStruct = struct();
            grainStruct.properties = propStruct;
            grainStruct.type = g.type;
        end

    end
    methods (Static)
        function isValid = checkInhibitedEnds(inhibitedEnds)
            validInhibitedEnds = {'Neither','Top','Bottom','Both'};
            if isnumeric(inhibitedEnds)
                error('inhibitedEnds parameter must not be numeric.');
            end
            
            if ismember(inhibitedEnds,validInhibitedEnds)
                isValid = true;
            else
                isValid = false;
                msg = {...
                    'Invalid inhibitedEnds parameter.',...
                    'Valid values are: Neither, Top, Bottom, Both',...
                    'Attempted value:'};

                error('%s\n\t%s\n\t%s %s', msg{1}, msg{2}, msg{3}, value);
            end
        end
    end
end