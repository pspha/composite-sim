classdef MoonBurner < Grain
    properties
        coreDiameter
        coreOffset
        diameter
        inhibitedEnds
        length
    end
    
    methods
        function g = MoonBurner(coreDiameter,coreOffset,diameter,inhibitedEnds,length)
            g.coreDiameter = coreDiameter;
            g.coreOffset = coreOffset;
            g.diameter = diameter;
            g.inhibitedEnds = inhibitedEnds;
            g.length = length;
        end
        function grainType = type(g)
            grainType = 'Moon Burner';
        end
        function isValid = check(g)
            isValid = g.checkMoonBurner(g.coreDiameter,g.coreOffset,g.diameter,g.inhibitedEnds,g.length);
        end
    end
    methods (Static)
        function isValid = checkMoonBurner(coreDiameter,coreOffset,diameter,inhibitedEnds,length)
            isValid = Grain.checkInhibitedEnds(inhibitedEnds);
            if coreDiameter == 0
                warning('Core diameter must not be 0.');
                isValid = false;
            end
            if coreDiameter >= diameter
                warning('Core diameter must be less than grain diameter.');
                isValid = false;
            end
            
            if coreOffset * 2 > diameter
                warning('Core offset should be less than or equal to grain radius.');
                isValid = false;
            end
        end
    end
end