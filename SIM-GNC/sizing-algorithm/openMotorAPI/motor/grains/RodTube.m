classdef RodTube < Grain
    properties
        coreDiameter
        diameter
        inhibitedEnds
        length
        rodDiameter
    end

    methods
        function g = RodTube(coreDiameter,diameter,inhibitedEnds,length,rodDiameter)
            g.coreDiameter = coreDiameter;
            g.diameter = diameter;
            g.inhibitedEnds = inhibitedEnds;
            g.length = length;
            g.rodDiameter = rodDiameter;
        end
        function grainType = type(g)
            grainType = 'Rod and Tube';
        end
        function isValid = check(g)
            isValid = g.checkRodTube(g.coreDiameter,g.diameter,g.inhibitedEnds,g.length,g.rodDiameter);
        end
        
    end
    methods (Static)
        function isValid = checkRodTube(coreDiameter,diameter,inhibitedEnds,length,rodDiameter)
            isValid = Grain.checkInhibitedEnds(inhibitedEnds);
            if coreDiameter == 0
                warning('Core diameter must not be 0.');
                isValid = false;
            end
            if coreDiameter >= diameter
                warning('Core diameter must be less than grain diameter.');
                isValid = false;
            end
            
            if rodDiameter >= coreDiameter
                warning('Rod diameter must be less than core diameter');
                isValid = false;
            end
        end
            
    end
end