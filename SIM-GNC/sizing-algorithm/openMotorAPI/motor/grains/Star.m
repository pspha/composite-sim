classdef Star < Grain
    properties
        diameter
        inhibitedEnds
        length
        numPoints
        pointLength
        pointWidth
    end
    
    methods
        function g = Star(diameter,inhibitedEnds,length,numPoints,...
                pointLength,pointWidth)
            g.diameter = diameter;
            g.inhibitedEnds = inhibitedEnds;
            g.length = length;
            g.numPoints = numPoints;
            g.pointLength = pointLength;
            g.pointWidth = pointWidth;
        end
        function grainType = type(g)
            grainType = 'Star Grain';
        end
        function isValid = check(g)
            isValid = g.checkStar(g.diameter,g.inhibitedEnds,g.length,g.numPoints,g.pointLength,g.pointWidth);
        end
    end
    methods (Static)
        function isValid = checkStar(diameter,inhibitedEnds,length,numPoints,pointLength,pointWidth)
            isValid = Grain.checkInhibitedEnds(inhibitedEnds);
            if numPoints == 0
                warning('Star grain has 0 points.');
                isValid = false;
            end

            if pointLength == 0
                warning('Point length must not be 0.');
                isValid = false;
            end
            if pointLength * 2 > diameter
                warning('Point length should be less than or equal to grain radius.');
                isValid = false;
            end

            if pointWidth == 0
                warning('Point width must not be 0.');
                isValid = false;
            end
        end
    end
end