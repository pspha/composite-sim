classdef Finocyl < Grain
    properties
        coreDiameter
        diameter
        finLength
        finWidth
        inhibitedEnds
        length
        numFins
    end
    
    methods
        function g = Finocyl(coreDiameter,diameter,finLength,finWidth,...
                inhibitedEnds,length,numFins)
            g.coreDiameter = coreDiameter;
            g.diameter = diameter;
            g.finLength = finLength;
            g.finWidth = finWidth;
            g.inhibitedEnds = inhibitedEnds;
            g.length = length;
            g.numFins = numFins;
        end
        function grainType = type(g)
            grainType = 'Finocyl';
        end
        function isValid = check(g)
            isValid = g.checkFinocyl(g.coreDiameter,g.diameter,g.finLength,...
                g.finWidth,g.inhibitedEnds,g.length,g.numFins);
        end
    end
    methods (Static)
        function isValid = checkFinocyl(coreDiameter,diameter,finLength,...
                finWidth,inhibitedEnds,length,numFins)
            isValid = Grain.checkInhibitedEnds(inhibitedEnds);
            if coreDiameter == 0
                warning('Core diameter must not be 0.');
                isValid = false;
            end
            if coreDiameter >= diameter
                warning('Core diameter must be less than grain diameter.');
                isValid = false;
            end

            if finLength == 0
                warning('Fin length must not be 0.');
                isValid = false;
            end
            if finLength * 2 > diameter
                warning('Fin length should be less than or equal to grain radius.');
                isValid = false;
            end
            coreWidth = coreDiameter + (2 * finLength);
            if coreWidth > diameter
                warning('Core radius plus fin length should be less than or equal to grain radius.');
                isValid = false;
            end

            if finWidth == 0
                warning('Fin width must not be 0.');
                isValid = false;
            end
            if numFins > 1
                radius = coreDiameter / 2;
                apothem = radius + finLength;
                sideLength = 2 * apothem * tan(pi / numFins);
                if sideLength < finWidth
                    warning('Fin tips intersect.');
                    isValid = false;
                end
            end
        end
    end
end
        