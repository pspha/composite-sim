classdef Motor
    %MOTOR Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        config
        grains
        nozzle
        propellant
    end
    
    properties (Access = private)
        type
        version
        pyMotor
        pyMotorLoaded
    end
    
    methods
        function m = Motor(fileName)
           motorStruct = loadYAML(fileName);
           m.config = motorStruct.data.config;
           m.grains = struct2Grain(motorStruct.data.grains);
           m.nozzle = motorStruct.data.nozzle;
           m.propellant = struct2Prop(motorStruct.data.propellant);
           m.type = motorStruct.type;
           m.version = motorStruct.version;
           
           m.pyMotorLoaded = false;
        end
        
        function motorStruct = getStruct(m)
            data = struct();
            data.config = m.config;
            data.grains = grain2Struct(m.grains);
            data.nozzle = m.nozzle;
            data.propellant = m.propellant.getStruct();
            
            motorStruct = struct();
            motorStruct.data = data;
            motorStruct.type = m.type;
            motorStruct.version = m.version;
        end
           
        function saveMotor(m,fileName)
            fmt = get(0,'Format');
            format long;
            ms = m.getStruct();
            ms.data.config.ambPressure = sprintf('%.8f',ms.data.config.ambPressure);
            ms.data.config.burnoutWebThres = sprintf('%.20f',ms.data.config.burnoutWebThres);
            ms.data.config.igniterPressure = sprintf('%.8f',ms.data.config.igniterPressure);
            ms.data.config.maxMassFlux = sprintf('%.8f',ms.data.config.maxMassFlux);
            ms.data.config.maxPressure = sprintf('%.8f',ms.data.config.maxPressure);

            format(fmt);
            
            saveYAML(fileName,ms);
        end
    end
end

