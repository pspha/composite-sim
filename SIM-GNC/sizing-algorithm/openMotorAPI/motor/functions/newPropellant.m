function p = newPropellant(name, density, tabs)
    p = Propellant();
    p.name = name;

    if nargin > 1
        p.density = density;
    end

    if nargin > 2
        p.tabs = tabs;
    end
end