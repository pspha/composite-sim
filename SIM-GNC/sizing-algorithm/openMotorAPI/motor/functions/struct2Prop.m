function prop = struct2Prop(propStruct)
    prop = newPropellant(propStruct.name,propStruct.density,propStruct.tabs);
    prop.editsEnabled = false;
    
end