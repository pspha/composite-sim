# openMotorAPI for MATLAB 0.0.2
An API for interfacing openMotor with Matlab. Designed for PSPHA. Written by Jeff Kaji.

## Overview
**openMotorAPI** imports and exports data from the files used by openMotor. openMotor natively saves all data in the YAML data format, either as *.ric* files for motors and as *.yaml* files for everything else. 

**openMotorAPI**  imports these files and converts them to either MATLAB structs or custom classes that can be used to edit, check and/or save values back to the files they were loaded in from. New structs and classes can be constructed from scratch and saved to files as well.

## Getting Started
In order to load and run openMotor, **openMotorAPI** uses the file *.settings.json* to save two paths used by the code:


* **APIPATH**: The system path to the "../openMotorAPI/" folder
* **OMPATH**: The system path to the folder that contains the python source for openMotor. This folder should contain:
    * ".venv/" [(virtual environment folder)](#Setting-up-the-virtual-environment)
    * "motorlib/" (folder)
    * "uilib/" (folder)
    * "app.py"
    * "main.py

**APIPATH** is set automatically when the settings file is generated. 

**OMPATH** must be manually set by calling the function ``setOMPATH(ompath)`` where ompath is the system path to the openMotor source code folder.

``setOMPATH`` also has an optional second parameter ``doError`` that is by default set to false. If ``setOMPATH`` is called with an invalid ``ompath`` parameter and ``doError`` is set to true, MATLAB will throw an error and code execution will stop. If ``doError`` is set to false, MATLAB will only throw a warning if ``ompath`` is invalid and code execution will continue. Calling ``setOMPATH`` with ``doError`` set to true would look like this: ``setOMPATH("C:\example\openMotor-0.4.0", true);``

## Loading in existing Motor files (.ric)
To load a Motor from a _.ric_ file, simply call the class constructor with the desired file as the parameter.
```matlab
>>  m = Motor('test.ric')

m = 

  Motor with properties:

        config: [1×1 struct]
        grains: {[1×1 BATES]}
        nozzle: [1×1 struct]
    propellant: [1×1 Propellant]
```

##### Note: The path to the file is not needed as long as the file resides in a folder added to the matlab path.

## Editing Motor Properties
Properties of the motor can easily be accessed and modified.
#### Example 1: Editing the Nozzle
```
>> m.nozzle

ans = 

  struct with fields:

       convAngle: 30   << Original value
        divAngle: 12
      efficiency: 0.9800
    erosionCoeff: 1.4736e-08
            exit: 0.0264
       slagCoeff: 0
          throat: 0.0093
    throatLength: 0.0069
    
>> m.nozzle.convAngle = 28;
>> m.nozzle

ans = 

  struct with fields:

       convAngle: 28   << Updated updated
        divAngle: 12
      efficiency: 0.9800
    erosionCoeff: 1.4736e-08
            exit: 0.0264
       slagCoeff: 0
          throat: 0.0093
    throatLength: 0.0069
```

#### Example 2: Editing the Configuration
```
>> m.config

ans = 

  struct with fields:

           ambPressure: 1.0132e+05
    burnoutThrustThres: 0.1000
       burnoutWebThres: 2.5400e-04
       igniterPressure: 1034250
                mapDim: 750
           maxMassFlux: 1.4065e+03
           maxPressure: 10342500
         minPortThroat: 2
              timestep: 0.0300   << Original value

>> m.config.timestep = 0.01;
>> m.config

ans = 

  struct with fields:

           ambPressure: 1.0132e+05
    burnoutThrustThres: 0.1000
       burnoutWebThres: 2.5400e-04
       igniterPressure: 1034250
                mapDim: 750
           maxMassFlux: 1.4065e+03
           maxPressure: 10342500
         minPortThroat: 2
              timestep: 0.0100   << Updated updated
```

## Loading Motor into OpenMotor
Loading a motor into an openMotor simulation is done by constructing a ``MotorSim`` with the desired ``Motor`` as the only parameter.
```
>> ms = MotorSim(m)

ms = 

  MotorSim with properties:

    pyMotor: [1×1 py.motorlib.motor.Motor]
    simData: []
    
>> ms = ms.stepSim()

ms = 

  MotorSim with properties:

    pyMotor: [1×1 py.motorlib.motor.Motor]
    simData: [1×11 table]

>> ms = ms.stepSim()

ms = 

  MotorSim with properties:

    pyMotor: [1×1 py.motorlib.motor.Motor]
    simData: [2×11 table]
```

``MotorSim`` objects have two methods:

* ms = stepSim(ms, ambPressure)
* value = isComplete(ms)

### ms = stepSim(ms, ambPressure)
#### ambPressure:
Optional parameter that updates the ambient pressure in the simulation. Would be used as ambient pressure changes with altitude.
### value = isComplete(ms)
Returns whether the openMotor simulation has finished.

### Example Execution
```
>> ms = MotorSim(m)

ms = 

  MotorSim with properties:

    pyMotor: [1×1 py.motorlib.motor.Motor]
    simData: []

>> while ~ms.isComplete()
       ms = ms.stepSim();
   end
>> ms

ms = 

  MotorSim with properties:

    pyMotor: [1×1 py.motorlib.motor.Motor]
    simData: [12×11 table]

>> ms.simData

ans =

  12×11 table

    time      kn       pressure      force      mass      massFlow    massFlux    regression      web       exitPressure     dThroat 
    ____    ______    __________    _______    _______    ________    ________    __________    ________    ____________    _________

      0     95.995    8.4026e+05          0    0.16217           0          0              0      0.0127            0               0
    0.1     97.048      8.54e+05     41.158    0.16217           0     49.555     0.00030566    0.012394        14075       0.0025169
    0.2     60.881      4.27e+05     21.159    0.15877    0.034012     48.445     0.00061295    0.012087       7037.5       0.0037753
    0.3     50.173    3.2028e+05     14.657    0.15531    0.034563     37.826     0.00085786    0.011842       5278.7       0.0047192
    0.4     43.982    2.6334e+05     10.727    0.15253    0.027807     33.771      0.0010808    0.011619       4340.2       0.0054953
    0.5     39.765    2.2669e+05     7.9675    0.14998    0.025505      31.11      0.0012899     0.01141       3736.3       0.0061634
    0.6     36.638    2.0071e+05     6.0299    0.14757    0.024088     29.121      0.0014889    0.011211       3307.9        0.006755
    0.7     34.192    1.8112e+05     4.3209    0.14526    0.023082     27.533      0.0016802     0.01102       2985.1       0.0072887
    0.8     32.209    1.6572e+05     2.9129    0.14303    0.022313     26.212      0.0018652    0.010835       2731.3       0.0077771
    0.9     30.556    1.5324e+05     1.7243    0.14086    0.021697     25.081      0.0020449    0.010655       2525.6       0.0082288
      1     29.149    1.4287e+05    0.70177    0.13874    0.021187     24.095        0.00222     0.01048       2354.7       0.0086498
    1.1     27.933     1.341e+05          0    0.13667    0.020756      23.22      0.0023912    0.010309       2210.2        0.009045

```