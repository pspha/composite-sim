function [stage_pin_spacing_multiplier, stage_overall_safety_factor] = SafetyFactors(stage_motor_casing_thickness, stage_motor_max_pressure, stage_motor_diameter)
    % Calculates several safety factors for the motor casing using the inputs:
    %   Stage Motor Casing Thickness
    %   Stage Motor Maximum Pressure
    %   Stage Motor Diameter
    
    % Adopted from a safety factor calculator from the previous PSP Solids
    % team
    
    % Optional outputs: stage_number_pins_shear, stage_number_pins_tear


% Set Constants
desiredSafetyFactor = 1.75;
pinDiameter = 0.250;            % in
pinShearForce = 6400;           % lb
casingYieldStrength = 40000;    % psi


% Pin Shear Failure Mode
forceClosure = pi * ((stage_motor_diameter - 2 * stage_motor_casing_thickness)/2)^2 * stage_motor_max_pressure;
forceClosureSF = forceClosure * desiredSafetyFactor;
stage_number_pins_shear = ceil(forceClosureSF / pinShearForce);

% Case Tear-out Failure Mode
areaBoltHole = stage_motor_casing_thickness * pinDiameter;
forcePinTear = areaBoltHole * 30000;
stage_number_pins_tear = ceil(forceClosureSF / forcePinTear);

% Pin Spacing
pinSpacing =  (2 * pi * stage_motor_diameter/2) / stage_number_pins_tear - pinDiameter;
stage_pin_spacing_multiplier = pinSpacing / pinDiameter;

% Casing Burst Failure Mode
detradeTubeStrength = 2/3 * casingYieldStrength;
burstPressure = 2 * detradeTubeStrength * stage_motor_casing_thickness / stage_motor_diameter;
stage_overall_safety_factor = burstPressure / stage_motor_max_pressure;

end

