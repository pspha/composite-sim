function [nose_cone_length,isValid] = NoseConeLength(absolute_length, upper_stage_length, interstage_length, lower_stage_length)
    % Calculates the rocket nose cone length using the inputs:
    %   Absolute Length
    %   Upper Stage Length
    %   Interstage Length
    %   Lower Stage Length
    
    nose_cone_length = absolute_length - upper_stage_length...
        - interstage_length - lower_stage_length;
    isValid = true;
end