function [stage_motor_casing_thickness,isValid] = StageMotorCasingThickness(stage_motor_diameter, stage_motor_max_pressure)
    % Determines the required thickness of a stage's motor casing using the inputs:
    %   Stage Motor Diameter
    %   Stage Motor Maximum Pressure
    
    temp_thickness = 0.25;
    [temp_spacing_multiplier, temp_safety_factor] = SafetyFactors(temp_thickness, stage_motor_diameter, stage_motor_max_pressure);
    
    if (temp_spacing_multiplier > 3 && temp_safety_factor > 2)
        stage_motor_casing_thickness = temp_thickness;
        isValid = true;
    else
        temp_thickness = temp_thickness + 0.125;
        [temp_spacing_multiplier, temp_safety_factor] = SafetyFactors(temp_thickness, stage_motor_diameter, stage_motor_max_pressure);
            if (temp_spacing_multiplier > 3 && temp_safety_factor > 2)
                stage_motor_casing_thickness = temp_thickness;
                isValid = true;
            else
                temp_thickness = temp_thickness + 0.125;
                [temp_spacing_multiplier, temp_safety_factor] = SafetyFactors(temp_thickness, stage_motor_diameter, stage_motor_max_pressure);
                if (temp_spacing_multiplier > 3 && temp_safety_factor > 2)
                    stage_motor_casing_thickness = temp_thickness;
                    isValid = true;
                else
                    stage_motor_casing_thickness = -1;
                    isValid = false;
                end
            end
    end

end