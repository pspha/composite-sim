function [stage_airframe_mass,isValid] = StageAirframeMass(outer_diameter, stage_length, stage_wall_thickness, airframe_material_density)
    % Calculates the mass of a stage's airframe using the inputs:
    %   Stage Length
    %   Stage Wall Thickness
    %   Airframe Material Density
    
    stage_airframe_mass = stage_length * pi * (outer_diameter^2 - ...
        (outer_diameter - stage_wall_thickness)^2) * ...
        airframe_material_density;
    isValid = true;
end