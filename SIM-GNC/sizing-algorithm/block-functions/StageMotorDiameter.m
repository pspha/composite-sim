function [stage_motor_diameter, isValid] = StageMotorDiameter(stage_inner_diameter)
    % Calculates the diameter of a stage's motor using the inputs:
    %   Stage Inner Diameter

stage_motor_diameter = stage_inner_diameter - 0.75;
isValid = true;
end

