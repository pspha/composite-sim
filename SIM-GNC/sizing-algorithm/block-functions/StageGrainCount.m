function [stage_grain_count,isValid] = StageGrainCount(stage_grain_length, stage_motor_length)
    % Calculates the nubmer of grains required by a stage's motor using the inputs:
    %   Stage Grain Length
    %   Stage Motor Length
    
    stage_grain_count = stage_motor_length./stage_grain_length;
    isValid = true;
end
