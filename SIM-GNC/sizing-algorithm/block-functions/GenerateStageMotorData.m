function [stage_motor_mass, stage_motor_max_pressure, isValid] = GenerateStageMotorData(stage_motor_diameter, stage_grain_length, stage_grain_count)
    % Generates the motor data using the openMotorAPI using:
    %   Stage Inner Diameter
    %   Stage Grain Length
    %   Stage Grain Count

%% Load in initial motor
m = Motor('test.ric');

%% Calculate modification parameters
core_diameter = stage_motor_diameter / 2;

%% Modify the motor for the given stage
% Must convert measurements from inches to meters
m.grains{1}.coreDiameter = core_diameter * 0.0254;
m.grains{1}.diameter = stage_motor_diameter * 0.0254;
m.grains{1}.length = stage_grain_length * 0.0254;
m.nozzle.throat = core_diameter/2 * 0.0254;
disp(m.nozzle)

% Scale the number of grains on the motor by the stage grain count
if stage_grain_count > 1
    for i = 2:stage_grain_count
        m.grains{i} = m.grains{1};
        m.grains{i}.inhibitedEnds = 'Neither';
    end
end

%% Simulate the motor
ms = MotorSim(m);
while ~ms.isComplete
    ms = ms.stepSim();
end

msOut = ms.simData;

%% Parse the required data from the simulation
% stage_motor_thrust_curve = timeseries(msOut.force, msOut.time);
% stage_motor_mass_curve = timeseries(msOut.mass, msOut.time);
stage_motor_mass = msOut.mass(1);
stage_motor_max_pressure = max(msOut.pressure) / 6895;

isValid = true;
end

