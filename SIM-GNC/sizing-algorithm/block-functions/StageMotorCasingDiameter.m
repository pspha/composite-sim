function [stage_motor_casing_diameter,isValid] = StageMotorCasingDiameter(stage_motor_diameter, stage_motor_casing_thickness)
    % Calculates the diameter of a stage's motor casing using the inputs:
    %   Stage Inner Diameter
    %   Stage Motor Casing Thickness
    
    stage_motor_casing_diameter = stage_motor_diameter + 2.*stage_motor_casing_thickness;
    isValid = true;
end