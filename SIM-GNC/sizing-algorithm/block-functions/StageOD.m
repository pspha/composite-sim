function [stage_outer_diameter,isValid] = StageOD(stage_inner_diameter, stage_wall_thickness)
    % Determines the outer diameter of a stage's airframe using the inputs:
    %   Stage Inner Diameter
    %   Stage Wall Thickness
    
    stage_outer_diameter = stage_inner_diameter + 2.*stage_wall_thickness;
    isValid = true;
end