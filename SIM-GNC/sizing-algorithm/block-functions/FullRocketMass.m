function [full_rocket_mass, isValid] = FullRocketMass(upper_airframe_mass, lower_airframe_mass, upper_motor_mass, lower_motor_mass, payload_max_mass, recovery_max_mass, avionics_max_mass, fin_mass)
    % Calculates the mass of the full rocket using the inputs:
    %   Maximum Payload Mass
    %   Maximum Recovery Mass
    %   Maximum Avionics Mass
    %   Fin Mass
    %   Upper Airframe Mass
    %   Lower Airframe Mass
    %   Upper Motor Mass
    %   Lower Motor Mass
    
    full_rocket_mass = upper_airframe_mass + lower_airframe_mass...
        + upper_motor_mass + lower_motor_mass + payload_max_mass...
        + recovery_max_mass + avionics_max_mass + fin_mass;
    isValid = true;
end
