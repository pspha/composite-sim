% Purdue Space Program High Altitude
% Sizing Algorithm
% MATLAB: Main Script

%% Load Python Environment 
cd('..')
addpath(genpath('sizing-algorithm'))
cd('sizing-algorithm')
settings = loadSettings();
pe = pyenv;
executable = sprintf('%s.venv/Scripts/python.exe',settings.OMPATH);
if ~strcmp(pe.Executable,strrep(executable,'/','\'))
    pyenv('Version',executable);
end

%% Load in the sizing algorithm Simulink Model
mdl = 'sizing_algorithm';
load_system(mdl);

rocket_in = [0, 6, 6, 20, 6, 0.5, 11, 0.25, 0.25, 4, 5];

results = sim(mdl);

simMass = results.rocket_masses;
simDim = results.rocket_dimensions;


%% Initialize a struct to store the data for the valid possible rocket designs
% rockets = struct('lowerStageID', {}, 'upperStageID', {}, 'absoluteLength', {}, ...
%                  'upperStageLength', {}, 'interstageLength', {}, 'lowerStageLength', {}, ...
%                  'upperStageWallThickness', {}, 'lowerStageWallThickness', {}, ...
%                  'upperMotorLength', {}, 'lowerMotorLength', {}, 'lowerStageOD', {}, ...
%                  'upperStageOD', {}, 'noseConeLength', {}, 'upperMotorCasingThickness', {}, ...
%                  'lowerMotorCasingThickness', {}, 'upperMotorCasingDiameter', {}, ...
%                  'lowerMotorCasingDiameter', {}, 'upperGrainCount', {}, ...
%                  'lowerGrainCount', {}, 'rocketMass', {}, 'massCurves', {}, ...
%                  'thrustCurves', {});
 
%% Begin the algorithm

 